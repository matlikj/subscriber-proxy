use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};
use std::{convert::Infallible, env, env::VarError, net::ToSocketAddrs};

async fn handle(_: Request<Body>) -> Result<Response<Body>, Infallible> {
    Ok(Response::new("Hello, World!".into()))
}

#[tokio::main]
async fn main() {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let service_listener_addr = get_env_or_else("SERVICE_LISTENER_ADDRESS", "127.0.0.1:8003");

    log::info!(
        "{} v{}:\n  SERVICE_LISTENER_ADDRESS: {}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION"),
        service_listener_addr
    );

    let addr = &service_listener_addr
        .to_socket_addrs()
        .unwrap()
        .next()
        .unwrap();

    let make_svc = make_service_fn(|_conn| async { Ok::<_, Infallible>(service_fn(handle)) });

    let server = Server::bind(addr).serve(make_svc);

    if let Err(e) = server.await {
        log::error!("server error: {}", e);
    }
}

fn get_env_or_else(env: &str, default: &str) -> String {
    match env::var(env) {
        Ok(value) => value,
        Err(VarError::NotPresent) => default.to_string(),
        Err(e) => panic!("Failed to parse ENV variable '{}': {}", env, e),
    }
}
