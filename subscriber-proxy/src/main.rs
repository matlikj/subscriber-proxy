#![warn(rust_2018_idioms)]

use std::env;

use tokio::net::TcpListener;
use tokio::task;
use tokio::time::Duration;

use proxy_core::config_constants::*;
use proxy_core::protocol::ProtocolResult;
use subscriber_proxy::worker_pool::{
    ClientListenerActor, WorkerPoolActor, WorkerPoolReaperActor, WorkerRegistrationActor,
};

// This class has been heavily built on https://github.com/tokio-rs/tokio/blob/master/examples/proxy.rs
// and https://github.com/tokio-rs/tokio-core/blob/master/examples/proxy.rs
#[tokio::main]
async fn main() -> ProtocolResult<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let config = ProxyConfig::load();

    let worker_pool_actor = WorkerPoolActor::new(config.max_worker_count);

    let worker_listener = TcpListener::bind(&config.worker_listener_address).await?;
    let secret = config.secret.clone();
    let protocol_timeout = Duration::from_millis(config.protocol_timeout_millis);
    let worker_pool_channel = worker_pool_actor.get_queue_command_sender();
    let worker_listener_actor = WorkerRegistrationActor::new(
        worker_listener,
        secret,
        protocol_timeout,
        worker_pool_channel,
    );

    let client_listener = TcpListener::bind(&config.client_listener_address).await?;
    let worker_pool_channel = worker_pool_actor.get_queue_command_sender();
    let worker_heartbeat_before_use = config.worker_heartbeat_before_use;
    let client_listener_actor = ClientListenerActor::new(
        client_listener,
        worker_pool_channel,
        worker_heartbeat_before_use,
    );

    let keep_alive_duration =
        Duration::from_millis(config.worker_heartbeat_frequency_millis as u64);
    let worker_pool_channel = worker_pool_actor.get_queue_command_sender();
    let worker_pool_reaper_actor =
        WorkerPoolReaperActor::new(keep_alive_duration, worker_pool_channel);

    let _worker_pool_actor_join = task::spawn(async move {
        worker_pool_actor.start().await;
    });

    let _worker_listener_actor_join = task::spawn(async move {
        worker_listener_actor.start().await;
    });

    let _worker_pool_reaper_actor_join = task::spawn(async move {
        worker_pool_reaper_actor.start().await;
    });

    let client_listener_actor_join = task::spawn(async move {
        client_listener_actor.start().await;
    });

    log::info!("{}", &config.to_string());

    client_listener_actor_join.await?;
    Ok(())
}

#[derive(Clone, Debug)]
struct ProxyConfig {
    client_listener_address: String,
    max_worker_count: usize,
    worker_heartbeat_frequency_millis: u128,
    worker_heartbeat_before_use: bool,
    protocol_timeout_millis: u64,
    secret: String,
    worker_listener_address: String,
}

impl ProxyConfig {
    fn load() -> ProxyConfig {
        let mut config = ProxyConfig {
            client_listener_address: "127.0.0.1:8001".to_owned(),
            max_worker_count: 1024,
            worker_heartbeat_frequency_millis: 10000,
            worker_heartbeat_before_use: true,
            protocol_timeout_millis: 300,
            secret: "This must be set at runtime".to_owned(),
            worker_listener_address: "127.0.0.1:8002".to_owned(),
        };

        for (key, value) in env::vars() {
            match &key {
                a if a == ENV_WORKER_LISTENER_ADDRESS => config.worker_listener_address = value,
                a if a == ENV_MAX_WORKER_COUNT => {
                    config.max_worker_count = value.parse::<usize>().unwrap()
                }
                a if a == ENV_WORKER_HEARTBEAT_FREQUENCY_MILLIS => {
                    config.worker_heartbeat_frequency_millis = value.parse::<u128>().unwrap()
                }
                a if a == ENV_WORKER_HEARTBEAT_BEFORE_USE => {
                    config.worker_heartbeat_before_use = value.parse::<bool>().unwrap()
                }
                a if a == ENV_PROTOCOL_TIMEOUT_MILLIS => {
                    config.protocol_timeout_millis = value.parse::<u64>().unwrap()
                }
                a if a == ENV_SECRET => config.secret = value,
                a if a == ENV_CLIENT_LISTENER_ADDRESS => config.client_listener_address = value,
                _ => (),
            }
        }

        config
    }
}

impl std::fmt::Display for ProxyConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let message = format!(
            "{} v{}:\n  CLIENT_LISTENER_ADDRESS: {}\n  WORKER_LISTENER_ADDRESS: {}\n  MAX_WORKER_COUNT: {}\n  WORKER_HEARTBEAT_FREQUENCY_MILLIS: {}\n  WORKER_HEARTBEAT_BEFORE_USE: {}\n  PROTOCOL_TIMEOUT_MILLIS: {}\n  SECRET: <hidden>",
            env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"), self.client_listener_address, self.worker_listener_address, self.max_worker_count, self.worker_heartbeat_frequency_millis, self.worker_heartbeat_before_use, self.protocol_timeout_millis
        );

        std::fmt::Display::fmt(message.as_str(), f)
    }
}
