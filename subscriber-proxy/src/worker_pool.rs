use proxy_core::protocol::{ProtocolError, ProtocolResult, RegisteredWorker};
use std::collections::vec_deque::VecDeque;
use std::ops::Add;
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::mpsc;
use tokio::sync::oneshot;
use tokio::time::{Duration, Instant};

/// A response object returned when calling the WorkerPoolActor with
/// QueueCommand::PopOnOrBeforeResponse
#[derive(Debug)]
pub struct PopOnOrBeforeResponse {
    entry: Option<RegisteredWorker>,
    next_entry_insert_time: Option<Instant>,
}

/// Commands that are passed to the WorkerPoolActor via message passing
/// to interact with the WorkerPool
#[derive(Debug)]
pub enum QueueCommand {
    Push(RegisteredWorker),
    Pop(oneshot::Sender<Option<RegisteredWorker>>),
    PopOnOrBefore(Instant, oneshot::Sender<PopOnOrBeforeResponse>),
    IsFull(oneshot::Sender<bool>),
}

/// An envelope object representing a worker's entry into the WorkerPool.
/// It contains additional metadata related to connection keep-alive state.
struct WorkerPoolEntry {
    worker: RegisteredWorker,
    time_pushed: Instant,
}

impl WorkerPoolEntry {
    fn new(worker: RegisteredWorker) -> WorkerPoolEntry {
        WorkerPoolEntry {
            worker,
            time_pushed: Instant::now(),
        }
    }
}

/// An object using the Actor model for maintaining WorkerPool state.
///
/// The mutable state can only be read or written by the actor, and
/// other objects, potentially on other threads, may only interact with
/// the pool indirectly by passinng messages, or in ths case QueueCommand
/// objects, over a multi-producer, single-consumer (mpsc) channel.
///
/// When processing a command, the actor interacts directly with the
/// mutable state and tries to spend as little time processing as possible.
/// As a result, any logic performed on workers that could take time to
/// complete must be performed either before pushing into the pool, or
/// after popping from the pool.
pub struct WorkerPoolActor {
    queue: VecDeque<WorkerPoolEntry>,
    queue_command_receiver: mpsc::Receiver<QueueCommand>,
    max_worker_count: usize,
    queue_command_sender: mpsc::Sender<QueueCommand>,
}

impl WorkerPoolActor {
    pub fn new(max_worker_count: usize) -> WorkerPoolActor {
        let (tx, rx) = mpsc::channel(1024);

        WorkerPoolActor {
            queue: VecDeque::<WorkerPoolEntry>::with_capacity(max_worker_count),
            queue_command_receiver: rx,
            max_worker_count,
            queue_command_sender: tx,
        }
    }

    pub async fn start(mut self) {
        // XXX: Is this a potential source for unexpected server termination?
        while let Some(cmd) = self.queue_command_receiver.recv().await {
            match cmd {
                QueueCommand::IsFull(sender) => self.is_full(sender),
                QueueCommand::Push(registered_worker) => self.push(registered_worker),
                QueueCommand::Pop(sender) => self.pop(sender),
                QueueCommand::PopOnOrBefore(boundary_time, sender) => {
                    self.pop_on_or_before(sender, boundary_time)
                }
            }
            // print!("\r{:>6}", self.queue.len());
        }
    }

    /// Provide the sender side of the message passing channel. This is the
    /// only way code outside the actor can interact with the worker pool.
    pub fn get_queue_command_sender(&self) -> mpsc::Sender<QueueCommand> {
        self.queue_command_sender.clone()
    }

    fn is_full(&self, sender: oneshot::Sender<bool>) {
        sender
            .send(self.queue.len() < self.max_worker_count)
            .unwrap();
    }

    fn push(&mut self, mut registered_worker: RegisteredWorker) {
        // Only add to the queue if there is space. Otherwise, forget the worker and let it get cleaned up.
        if self.queue.len() < self.max_worker_count {
            self.queue
                .push_back(WorkerPoolEntry::new(registered_worker));
            log::debug!("Added to queue. Now containing: {}", self.queue.len());
        } else {
            log::debug!("Dropping push. Queue full.");
            tokio::spawn(async move {
                registered_worker.shutdown().await;
            });
        }
    }

    fn pop(&mut self, sender: oneshot::Sender<Option<RegisteredWorker>>) {
        if let Some(entry) = self.queue.pop_front() {
            if let Err(Some(registered_worker)) = sender.send(Some(entry.worker)) {
                let to_queue = WorkerPoolEntry {
                    worker: registered_worker,
                    time_pushed: entry.time_pushed,
                };
                self.queue.push_front(to_queue);
                log::debug!("Failed to deliver worker, so re-queueing it.");
            } else {
                log::debug!("Popped from queue. Now containing: {}", self.queue.len());
            }
        } else {
            sender.send(None).unwrap();
        }
    }

    fn pop_on_or_before(
        &mut self,
        sender: oneshot::Sender<PopOnOrBeforeResponse>,
        boundary_time: Instant,
    ) {
        if let Some(first) = self.queue.pop_front() {
            if first.time_pushed <= boundary_time {
                let next_entry_insert_time = if let Some(second) = self.queue.pop_front() {
                    let result = Some(second.time_pushed);
                    self.queue.push_front(second);
                    result
                } else {
                    None
                };

                let result = PopOnOrBeforeResponse {
                    entry: Some(first.worker),
                    next_entry_insert_time,
                };
                log::debug!("After {:?}. Now containing {}", result, self.queue.len());

                sender.send(result).unwrap();
                return;
            } else {
                self.queue.push_back(first);
            }
        }
        let result = PopOnOrBeforeResponse {
            entry: None,
            next_entry_insert_time: None,
        };
        log::debug!("After {:?}. Now containing {}", result, self.queue.len());
        let _ignored = sender.send(result);
    }
}

pub struct WorkerRegistrationActor {
    listener: TcpListener,
    secret: String,
    protocol_timeout: Duration,
    worker_pool: mpsc::Sender<QueueCommand>,
}

impl WorkerRegistrationActor {
    pub fn new(
        listener: TcpListener,
        secret: String,
        protocol_timeout: Duration,
        worker_pool: mpsc::Sender<QueueCommand>,
    ) -> WorkerRegistrationActor {
        WorkerRegistrationActor {
            listener,
            secret,
            protocol_timeout,
            worker_pool,
        }
    }

    pub async fn start(self) {
        loop {
            let (connection, addr) = self.listener.accept().await.unwrap();
            let worker_listener_worker = WorkerRegistrationWorker::new(
                connection,
                self.secret.clone(),
                self.protocol_timeout,
                self.worker_pool.clone(),
            );
            tokio::spawn(async move {
                match worker_listener_worker.listen_for_worker().await {
                    Ok(_) => log::debug!("Received worker connection at address: {}", addr),
                    Err(e) => log::warn!("Worker connection at {} rejected; error={}", addr, e),
                }
            });
        }
    }
}

struct WorkerRegistrationWorker {
    connection: TcpStream,
    secret: String,
    protocol_timeout: Duration,
    worker_pool: mpsc::Sender<QueueCommand>,
}

impl WorkerRegistrationWorker {
    fn new(
        connection: TcpStream,
        secret: String,
        protocol_timeout: Duration,
        worker_pool: mpsc::Sender<QueueCommand>,
    ) -> WorkerRegistrationWorker {
        WorkerRegistrationWorker {
            connection,
            secret,
            protocol_timeout,
            worker_pool,
        }
    }

    async fn listen_for_worker(self) -> ProtocolResult<()> {
        // Note that self.worker_pool.send(...) should never fail unless the pool doesn't exist, and we should panic
        // However, interacting with a remote server's connection still requires error handling
        let (tx, rx) = oneshot::channel::<bool>();
        self.worker_pool
            .send(QueueCommand::IsFull(tx))
            .await
            .unwrap();
        if let Ok(true) = rx.await {
            let mut worker = RegisteredWorker::new(self.connection, self.protocol_timeout);
            worker.verify_auth(self.secret.clone()).await?;
            self.worker_pool
                .send(QueueCommand::Push(worker))
                .await
                .unwrap();
        }
        Ok(())
    }
}

pub struct ClientListenerActor {
    listener: TcpListener,
    worker_pool: mpsc::Sender<QueueCommand>,
    worker_heartbeat_before_use: bool,
}

impl ClientListenerActor {
    pub fn new(
        listener: TcpListener,
        worker_pool: mpsc::Sender<QueueCommand>,
        worker_heartbeat_before_use: bool,
    ) -> ClientListenerActor {
        ClientListenerActor {
            listener,
            worker_pool,
            worker_heartbeat_before_use,
        }
    }

    pub async fn start(self) {
        loop {
            let (client_socket, addr) = match self.listener.accept().await {
                Ok(s) => s,
                Err(e) => panic!("Failed to accept client socket; error={}", e),
            };
            log::debug!("Received client connection at address: {}", addr);

            let transfer_worker = ClientListenerTransferWorker::new(
                client_socket,
                self.worker_pool.clone(),
                self.worker_heartbeat_before_use,
            );
            tokio::spawn(async move {
                if let Err(e) = transfer_worker.listen_for_client().await {
                    log::warn!("Failed to handle client socket; error={}", e);
                }
            });
        }
    }
}

struct ClientListenerTransferWorker {
    connection: TcpStream,
    worker_pool: mpsc::Sender<QueueCommand>,
    worker_heartbeat_before_use: bool,
}

impl ClientListenerTransferWorker {
    fn new(
        connection: TcpStream,
        worker_pool: mpsc::Sender<QueueCommand>,
        worker_heartbeat_before_use: bool,
    ) -> ClientListenerTransferWorker {
        ClientListenerTransferWorker {
            connection,
            worker_pool,
            worker_heartbeat_before_use,
        }
    }

    async fn listen_for_client(mut self) -> ProtocolResult<()> {
        match self.get_worker().await {
            Some(mut worker) => {
                worker.proxy(self.connection).await?;
                Ok(())
            }
            None => Err(Box::new(ProtocolError::from(
                "QueueCommand::Pop failed. No workers available".to_string(),
            ))),
        }
    }

    async fn get_worker(&mut self) -> Option<RegisteredWorker> {
        loop {
            let (tx, rx) = oneshot::channel::<Option<RegisteredWorker>>();
            self.worker_pool.send(QueueCommand::Pop(tx)).await.unwrap();
            match rx.await.unwrap() {
                Some(mut worker) => {
                    if !self.worker_heartbeat_before_use || worker.send_ping().await.is_ok() {
                        return Some(worker);
                    }
                }
                None => return None,
            }
        }
    }
}

/// An actor that removes workers from the WorkerPoolActor who have broken
/// connections.
///
/// One worker at a time will be popped, checked via a heartbeat, and only
/// if the heartbeat completes without error, push the worker back into the
/// worker pool.
pub struct WorkerPoolReaperActor {
    keep_alive_frequency: Duration,
    worker_pool: mpsc::Sender<QueueCommand>,
}

impl WorkerPoolReaperActor {
    pub fn new(
        keep_alive_frequency: Duration,
        worker_pool: mpsc::Sender<QueueCommand>,
    ) -> WorkerPoolReaperActor {
        WorkerPoolReaperActor {
            keep_alive_frequency,
            worker_pool,
        }
    }

    pub async fn start(self) {
        let mut next_timeout = Instant::now();
        let worker_pool = self.worker_pool.clone();

        loop {
            let (tx, rx) = oneshot::channel::<PopOnOrBeforeResponse>();
            worker_pool
                .send(QueueCommand::PopOnOrBefore(next_timeout, tx))
                .await
                .unwrap();
            if let Ok(result) = rx.await {
                if result.entry.is_some() {
                    self.do_heartbeat(worker_pool.clone(), result.entry.unwrap())
                        .await;
                }
                next_timeout = self.delay_loop(result.next_entry_insert_time).await;
            }
        }
    }

    async fn delay_loop(&self, next_entry_inserted_time: Option<Instant>) -> Instant {
        let base_time = next_entry_inserted_time.unwrap_or_else(Instant::now);
        let delay_until_instant = base_time.add(self.keep_alive_frequency);
        tokio::time::sleep_until(delay_until_instant).await;
        delay_until_instant
    }

    async fn do_heartbeat(
        &self,
        worker_pool: mpsc::Sender<QueueCommand>,
        mut entry: RegisteredWorker,
    ) {
        if entry.send_ping().await.is_ok() {
            worker_pool.send(QueueCommand::Push(entry)).await.unwrap();
        } else {
            entry.shutdown().await;
            log::debug!("Worker failed keep-alive ping. Dropping.")
        }
    }
}
