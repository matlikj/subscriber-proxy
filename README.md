## Release Process

https://dev.to/sharkdp/my-release-checklist-for-rust-programs-1m33

## Building a Release for Multiple Architectures (cross building)
This documentation assumes [rustup](https://rustup.rs/) is already installed
on your machine.

Cross compiling Rust to multiple platforms is made fairly easy through use of
the [cross](https://github.com/rust-embedded/cross) command. It is a wrapper
around `cargo` to perform all the same build operations, but in a Docker image
specific to the desired architecture or build artifact.

First, make sure you install cross:

    cargo install cross

Then you can execute the build for any of the supported
[targets](https://github.com/rust-embedded/cross#supported-targets). 

The following will build statically linked binaries using the musl libraries
instead of gnu libc, which is always linked. 

    cross build --release --target x86_64-unknown-linux-musl
    cross test --release --target x86_64-unknown-linux-musl

The following will build a binary for Windows:

    cross build --release --target x86_64-pc-windows-gnu
    cross test --release --target x86_64-pc-windows-gnu

Note that not all targets support testing, as is the case for the musl build
at the time of this writing.

### Apple M1 silicon

Unfortunately, the `cross` application does not work for building executables
for Apple silicon yet. There are some hacks for cross compilation from Linux
to Apple, but it should be far simpler to just build the M1 compatible binary
on Apple hardware. To do this,

Install the tooling required to build for Apple's x86_64 architecture. The M1
silicon can execute this:

    rustup target install x86_64-apple-darwin

Then build and test natively:

    cargo build --release --target x86_64-apple-darwin
    cargo test --release --target x86_64-apple-darwin