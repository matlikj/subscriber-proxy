#![warn(rust_2018_idioms)]

use std::env;

use tokio::net::TcpStream;

use proxy_core::circuit_breaker::BlockingCircuitBreaker;
use proxy_core::config_constants::*;
use proxy_core::protocol::{ProtocolResult, Worker};
use std::time::Duration;

#[tokio::main]
async fn main() -> ProtocolResult<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let config = WorkerAgentConfig::load();

    log::info!("{}", config.clone().to_string());

    let circuit_breaker = BlockingCircuitBreaker::new(
        config.circuit_breaker_fail_count,
        config.circuit_breaker_retry_delay_millis,
    );

    iterate(config, circuit_breaker).await;

    Ok(())
}

async fn iterate(config: WorkerAgentConfig, circuit_breaker: BlockingCircuitBreaker) {
    use futures::stream::{repeat, StreamExt};

    repeat(())
        .for_each_concurrent(Option::Some(config.worker_count), |()| {
            execute_worker_no_return(config.clone(), circuit_breaker.clone())
        })
        .await
}

async fn execute_worker_no_return(
    config: WorkerAgentConfig,
    circuit_breaker: BlockingCircuitBreaker,
) {
    tokio::spawn(async move {
        if let Err(e) = execute_worker(config, circuit_breaker).await {
            log::warn!("Failed to transfer; error={}", e);
        };
    })
    .await
    .unwrap();
}

async fn execute_worker(
    config: WorkerAgentConfig,
    circuit_breaker: BlockingCircuitBreaker,
) -> ProtocolResult<()> {
    let worker_listener_address = config.worker_listener_address.clone();

    let inbound = circuit_breaker
        .run(|| async {
            let r = TcpStream::connect(worker_listener_address).await;
            match r {
                Ok(x) => Ok(x),
                Err(err) => Err(Box::new(err)),
            }
        })
        .await?;

    let mut worker = Worker::new(
        inbound,
        config.service_listener_address.clone(),
        config.secret.clone(),
        Duration::from_millis(config.protocol_timeout_millis),
    );

    loop {
        log::debug!("waiting for work");
        let keep_listening = worker.listen_for_work().await?;
        if !keep_listening {
            log::debug!("worker terminated");
            break;
        }
    }

    Ok(())
}

#[derive(Clone)]
struct WorkerAgentConfig {
    circuit_breaker_fail_count: u64,
    circuit_breaker_retry_delay_millis: u64,
    protocol_timeout_millis: u64,
    secret: String,
    service_listener_address: String,
    worker_count: usize,
    worker_listener_address: String,
}

impl WorkerAgentConfig {
    pub fn load() -> WorkerAgentConfig {
        let mut config = WorkerAgentConfig {
            circuit_breaker_fail_count: 3,
            circuit_breaker_retry_delay_millis: 10000,
            protocol_timeout_millis: 300,
            secret: "abcdefghijklmnopqrstuvwxyz".to_owned(),
            service_listener_address: "127.0.0.1:8003".to_owned(),
            worker_count: 10_usize,
            worker_listener_address: "127.0.0.1:8002".to_owned(),
        };

        for (key, value) in env::vars() {
            match &key {
                a if a == ENV_CIRCUIT_BREAKER_FAIL_COUNT => {
                    config.circuit_breaker_fail_count = value.parse::<u64>().unwrap()
                }
                a if a == ENV_CIRCUIT_BREAKER_RETRY_DELAY_MILLIS => {
                    config.circuit_breaker_retry_delay_millis = value.parse::<u64>().unwrap()
                }
                a if a == ENV_PROTOCOL_TIMEOUT_MILLIS => {
                    config.protocol_timeout_millis = value.parse::<u64>().unwrap()
                }
                a if a == ENV_SECRET => config.secret = value,
                a if a == ENV_SERVICE_LISTENER_ADDRESS => config.service_listener_address = value,
                a if a == ENV_WORKER_LISTENER_ADDRESS => config.worker_listener_address = value,
                a if a == ENV_WORKER_COUNT => config.worker_count = value.parse::<usize>().unwrap(),
                _ => (),
            }
        }

        config
    }
}

impl std::fmt::Display for WorkerAgentConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let message = format!(
            "{} v{}:\n  WORKER_LISTENER_ADDRESS: {}\n  SERVICE_LISTENER_ADDRESS: {}\n  WORKER_COUNT: {}\n  CIRCUIT_BREAKER_FAIL_COUNT: {}\n  CIRCUIT_BREAKER_RETRY_DELAY_MILLIS: {}\n  PROTOCOL_TIMEOUT_MILLIS: {}\n  SECRET: <hidden>",
            env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"), self.worker_listener_address, self.service_listener_address, self.worker_count, self.circuit_breaker_fail_count, self.circuit_breaker_retry_delay_millis, self.protocol_timeout_millis
        );

        std::fmt::Display::fmt(message.as_str(), f)
    }
}
