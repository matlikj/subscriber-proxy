#![warn(rust_2018_idioms)]
use std::collections::HashMap;
use std::{
    process::{Child, Command},
    sync::{Arc, Condvar, Mutex},
    thread,
};

// Cleanup was taken from https://stackoverflow.com/questions/57860613/how-to-add-a-shutdown-hook-to-a-rust-program
#[derive(Debug, Default)]
pub struct Cleanup {
    hooks: Vec<thread::JoinHandle<()>>,
    run: Arc<Mutex<bool>>,
    go: Arc<Condvar>,
}

impl Cleanup {
    pub fn add(&mut self, f: impl FnOnce() + Send + 'static) {
        let run = self.run.clone();
        let go = self.go.clone();

        let t = thread::spawn(move || {
            let mut run = run.lock().unwrap();

            while !*run {
                run = go.wait(run).unwrap();
            }

            f();
        });
        self.hooks.push(t);
    }
}

impl Drop for Cleanup {
    fn drop(&mut self) {
        eprintln!("Starting final cleanup");

        *self.run.lock().unwrap() = true;
        self.go.notify_all();

        for h in self.hooks.drain(..) {
            h.join().unwrap();
        }

        eprintln!("Final cleanup complete");
    }
}

//fn main() {
//    let mut cleanup = Cleanup::default();
//
//    cleanup.add(|| {
//        eprintln!("Cleanup #1");
//    });
//
//    cleanup.add(|| {
//        eprintln!("Cleanup #2");
//    });
//
//    panic!("Oh no!");
//}

#[derive(Debug)]
pub struct IntegrationResource {
    process: Child,
}

impl IntegrationResource {
    pub fn new(executable_name: &str, envs: &HashMap<String, String>) -> IntegrationResource {
        let absolute_path = build_exec_path(executable_name);
        let failure_message = format!(
            "Failed to start IntegrationResource for path '{}'",
            absolute_path
        );
        let process = Command::new(absolute_path)
            .envs(envs)
            .spawn()
            .unwrap_or_else(|_| panic!("{}", failure_message));

        // TODO: Wait for a log output instead of blindly waiting for some duration
        let pause_duration = std::time::Duration::from_millis(50);
        std::thread::sleep(pause_duration);

        IntegrationResource { process }
    }

    pub fn kill(&mut self) {
        match self.process.kill() {
            Ok(()) => (),
            Err(e) => {
                println!("Failed to kill child process; error={}", e);
            }
        }
    }
}

/// Allow automatic resource management to kill the child process when it goes out of scope, including
/// when the program exits abnormally.
impl Drop for IntegrationResource {
    fn drop(&mut self) {
        self.kill();
    }
}

/// This ensures the executable path takes the build target into account
pub fn build_exec_path(exec_name: &str) -> String {
    let exe_path = std::env::current_exe().unwrap();
    let parent_dir = exe_path.parent().unwrap();
    let parent_dir2 = parent_dir.parent().unwrap();
    [parent_dir2.to_str().unwrap(), "/", exec_name].concat()
}
