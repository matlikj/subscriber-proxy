#![warn(rust_2018_idioms)]
use it_tests::build_exec_path;

fn main() {
    println!("Hello, world!");
    let string = build_exec_path("example_service");
    println!("output: {}", string);
}
