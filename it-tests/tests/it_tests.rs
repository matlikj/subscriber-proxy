#![warn(rust_2018_idioms)]

use futures::stream::{repeat, StreamExt};
use it_tests::IntegrationResource;
use proxy_core::config_constants::*;
use proxy_core::protocol::Method;
use std::collections::HashMap;
use std::io::{Read, Write};
use std::net::{Ipv4Addr, SocketAddrV4, TcpListener, TcpStream};
use std::time::Duration;
use tokio::runtime::Runtime;

const ENV_RUST_LOG: &str = "RUST_LOG";
const MAX_WORKER_COUNT: usize = 2;
const WORKER_COUNT: usize = 2;
const HELLO_WORLD: &str = "Hello, World!";

fn get_envs() -> HashMap<String, String> {
    let mut envs = HashMap::new();
    envs.insert(ENV_RUST_LOG.to_owned(), "error".to_owned());
    envs.insert(ENV_CLIENT_LISTENER_ADDRESS.to_owned(), build_address());
    envs.insert(ENV_WORKER_LISTENER_ADDRESS.to_owned(), build_address());
    envs.insert(ENV_SERVICE_LISTENER_ADDRESS.to_owned(), build_address());
    envs.insert(
        ENV_MAX_WORKER_COUNT.to_owned(),
        MAX_WORKER_COUNT.to_string(),
    );
    envs.insert(ENV_WORKER_COUNT.to_owned(), WORKER_COUNT.to_string());
    envs.insert(
        ENV_WORKER_HEARTBEAT_FREQUENCY_MILLIS.to_owned(),
        "300".to_string(),
    );
    envs.insert(ENV_SECRET.to_owned(), "ThisIsMySecret".to_string());
    envs.insert(
        ENV_CIRCUIT_BREAKER_FAIL_COUNT.to_owned(),
        "10000".to_string(),
    );
    envs.insert(
        ENV_CIRCUIT_BREAKER_RETRY_DELAY_MILLIS.to_owned(),
        "10000".to_string(),
    );
    envs.insert(ENV_PROTOCOL_TIMEOUT_MILLIS.to_owned(), "1000".to_string());
    envs
}

fn build_address() -> String {
    let loopback = Ipv4Addr::new(127, 0, 0, 1);
    let addr = SocketAddrV4::new(loopback, 0);
    let listener = TcpListener::bind(addr).unwrap();
    let local_addr = listener.local_addr().unwrap();
    local_addr.to_string()
}

async fn blind_async_query_or_panic(url: String) {
    let _body = match reqwest::get(url.as_str()).await {
        Ok(response) => response.text().await.unwrap(),
        Err(e) => panic!("Failed to get result; error={}", e),
    };
}

fn get_query_url(env: &HashMap<String, String>) -> String {
    let address = env.get(ENV_CLIENT_LISTENER_ADDRESS).unwrap();
    ["http://", address].concat()
}

#[test]
fn services_should_service_requests() {
    let envs = get_envs();
    let _service = IntegrationResource::new("example-service", &envs);
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);
    let _agent = IntegrationResource::new("subscriber-worker-agent", &envs);

    let body = reqwest::blocking::get(get_query_url(&envs).as_str())
        .unwrap()
        .text()
        .unwrap();
    assert_eq!(body, HELLO_WORLD);
}

#[test]
fn services_should_handle_many_requests() {
    let envs = get_envs();
    let _service = IntegrationResource::new("example-service", &envs);
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);
    let _agent = IntegrationResource::new("subscriber-worker-agent", &envs);

    let workers: Option<usize> = Option::Some(WORKER_COUNT);

    let completion = repeat(get_query_url(&envs))
        .take(MAX_WORKER_COUNT)
        .for_each_concurrent(workers, |url| blind_async_query_or_panic(url));

    let rt = Runtime::new().unwrap();
    let _resp = rt.block_on(completion);
}

#[test]
#[should_panic(expected = "Failed to get result; error=error sending request for url")]
fn services_should_reject_exessive_requests() {
    let envs = get_envs();
    let _service = IntegrationResource::new("example-service", &envs);
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);
    let _agent = IntegrationResource::new("subscriber-worker-agent", &envs);

    let workers: Option<usize> = Option::Some(WORKER_COUNT);

    let completion = repeat(get_query_url(&envs))
        .take(MAX_WORKER_COUNT + 1)
        .for_each_concurrent(workers, |url| blind_async_query_or_panic(url));

    let rt = Runtime::new().unwrap();
    let _resp = rt.block_on(completion);
}

#[test]
fn proxy_should_clear_out_dead_worker_connections() {
    let mut envs = get_envs();
    envs.insert(ENV_WORKER_COUNT.to_owned(), "1".to_owned());

    let _service = IntegrationResource::new("example-service", &envs);
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);
    let mut agent1 = IntegrationResource::new("subscriber-worker-agent", &envs);
    agent1.kill();
    let _agent2 = IntegrationResource::new("subscriber-worker-agent", &envs);

    let body = reqwest::blocking::get(get_query_url(&envs).as_str())
        .unwrap()
        .text()
        .unwrap();
    assert_eq!(body, HELLO_WORLD);
}

#[test]
fn proxy_should_reject_client_when_no_workers_available() {
    let envs = get_envs();
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);

    let result: Result<_, _> = reqwest::blocking::get(get_query_url(&envs).as_str());
    assert!(
        result.is_err(),
        "Query should fail with a broken connection"
    );
}

#[test]
fn proxy_should_reject_excessive_worker_connections() {
    let envs = get_envs();
    let _service = IntegrationResource::new("example-service", &envs);
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);
    let _agent = IntegrationResource::new("subscriber-worker-agent", &envs);

    // We cannot initiate registration of a new worker thread
    let address = envs.get(ENV_WORKER_LISTENER_ADDRESS).unwrap();
    let url = ["http://", address].concat();
    let result: Result<_, _> = reqwest::blocking::get(url.as_str());
    assert!(
        result.is_err(),
        "Query should fail with a broken connection"
    );

    // Prove the whole stack still works
    let body = reqwest::blocking::get(get_query_url(&envs).as_str())
        .unwrap()
        .text()
        .unwrap();
    assert_eq!(body, HELLO_WORLD);
}

fn get_connection(address: &String) -> TcpStream {
    let connection = TcpStream::connect(address).unwrap();
    connection
        .set_read_timeout(Some(Duration::from_secs(2)))
        .unwrap();
    connection
}

fn request_protocol_method(address: &String) -> Method {
    let connection = get_connection(address);
    read_protocol_method(&connection)
}

fn read_protocol_method(mut connection: &TcpStream) -> Method {
    let mut buf = [0 as u8];
    let _ignored_error = connection.read_exact(&mut buf);

    // The default is 0, which will translate to invalid
    let byte = buf.get(0).unwrap().to_owned();
    Method::from_byte(byte)
}

fn read_u32(mut connection: &TcpStream) -> u32 {
    let mut buf = vec![0u8; 4];
    connection.read_exact(buf.as_mut_slice()).unwrap();
    let mut value: u32 = 0;
    let mut byte_shift: u32 = 4;
    for b in buf {
        byte_shift = byte_shift - 1;
        value = value | (b as u32) << (byte_shift * 4);
    }
    value
}

#[test]
fn proxy_should_immediately_ask_worker_for_auth() {
    let envs = get_envs();
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);

    let address = envs.get(ENV_WORKER_LISTENER_ADDRESS).unwrap();
    let proxy_protocol = request_protocol_method(address);

    assert_eq!(proxy_protocol, Method::Auth);
}

#[test]
fn proxy_should_timeout_delayed_auth_response() {
    let mut envs = get_envs();
    envs.insert(ENV_PROTOCOL_TIMEOUT_MILLIS.to_owned(), "1".to_string());
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);

    let address = envs.get(ENV_WORKER_LISTENER_ADDRESS).unwrap();
    let mut connection = get_connection(address);

    assert_eq!(read_protocol_method(&connection), Method::Auth);
    let mut buf = vec![0u8; read_u32(&connection) as usize];
    connection.read_exact(buf.as_mut_slice()).unwrap();

    std::thread::sleep(Duration::from_millis(100));

    // Any sent response should fail
    let mut buf = vec![0u8; 4];
    connection.write(buf.as_mut_slice()).unwrap();

    // Read the message
    assert_eq!(read_protocol_method(&connection), Method::Invalid);
    let mut message = Vec::new();
    connection.read_to_end(&mut message).unwrap();

    assert_eq!(b"", message.as_slice());

    // TODO: Need to better recognize the timeout error
}

#[test]
fn proxy_should_reject_bad_auth_response_with_message() {
    let envs = get_envs();
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);

    let address = envs.get(ENV_WORKER_LISTENER_ADDRESS).unwrap();
    let mut connection = get_connection(address);

    // Read Auth method
    assert_eq!(read_protocol_method(&connection), Method::Auth);

    // read nonce
    let nonce_length = read_u32(&connection);
    let mut buf = vec![0u8; nonce_length as usize];
    connection.read_exact(buf.as_mut_slice()).unwrap();

    // Send a bad response (zero length and no message)
    let mut buf = vec![0u8; 4];
    connection.write(buf.as_mut_slice()).unwrap();

    // Read the message
    assert_eq!(read_protocol_method(&connection), Method::Invalid);
    let mut message = Vec::new();
    connection.read_to_end(&mut message).unwrap();

    assert_eq!(b"Failed Auth", message.as_slice());
}

#[test]
fn proxy_should_gc_dead_worker_connections_on_worker_subscription() {
    let envs = get_envs();
    let _service = IntegrationResource::new("example-service", &envs);
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);
    let mut agent = IntegrationResource::new("subscriber-worker-agent", &envs);
    agent.kill();

    let address = envs.get(ENV_WORKER_LISTENER_ADDRESS).unwrap();

    // Our first attempt will be rejected because the GC period has not passed
    let proxy_protocol = request_protocol_method(address);
    assert_eq!(proxy_protocol, Method::Invalid);

    // After the GC period elapses, the GC will allow us to add connections without "consuming" bad ones.
    let pause_duration = std::time::Duration::from_millis(1000);
    std::thread::sleep(pause_duration);
    let proxy_protocol = request_protocol_method(address);
    assert_eq!(proxy_protocol, Method::Auth);
}

#[test]
fn agent_should_throttle_connection_recreation_when_proxy_rejects_connections() {
    let mut envs = get_envs();
    envs.insert(
        ENV_CIRCUIT_BREAKER_RETRY_DELAY_MILLIS.to_owned(),
        "1000".to_owned(),
    );
    envs.insert(ENV_CIRCUIT_BREAKER_FAIL_COUNT.to_owned(), "1".to_string());

    let _service = IntegrationResource::new("example-service", &envs);
    let _agent = IntegrationResource::new("subscriber-worker-agent", &envs);

    // Let the agent fail till circuit breaker opens, then make the proxy available
    std::thread::sleep(Duration::from_millis(300));
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);

    // Before the timeout, we fail immediately
    let result: Result<_, _> = reqwest::blocking::get(get_query_url(&envs).as_str());
    assert!(
        result.is_err(),
        "Query should fail with a broken connection"
    );

    // After the circuit breaker retry delay, we should succeed.
    std::thread::sleep(Duration::from_millis(1000));
    let body = reqwest::blocking::get(get_query_url(&envs).as_str())
        .unwrap()
        .text()
        .unwrap();
    assert_eq!(body, HELLO_WORLD);
}

#[test]
fn agent_subscription_requires_authentication() {
    let mut envs = get_envs();
    let _service = IntegrationResource::new("example-service", &envs);
    let _proxy = IntegrationResource::new("subscriber-proxy", &envs);

    // This will effectively break the auth handshake
    envs.insert(ENV_SECRET.to_string(), "BOGUS".to_string());
    let _agent = IntegrationResource::new("subscriber-worker-agent", &envs);

    let result: Result<_, _> = reqwest::blocking::get(get_query_url(&envs).as_str());
    assert!(
        result.is_err(),
        "Query should fail with a broken connection"
    );
}
