use std::error::Error;
use std::pin::Pin;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant};

use crossbeam::atomic::AtomicCell;
use crossbeam::queue::ArrayQueue;
use futures::task::{Context, Poll, Waker};
use futures::Future;
use std::thread;

#[derive(Debug, Clone)]
pub struct BlockingCircuitBreakerState {
    block_on_failure_count: u64,
    retry_period_millis: u64,
    failure_count: Arc<AtomicU64>,
    last_failure: Arc<AtomicCell<Instant>>,
    wakers: Arc<ArrayQueue<Waker>>,
}

impl BlockingCircuitBreakerState {
    fn new(block_on_failure_count: u64, retry_period_millis: u64) -> BlockingCircuitBreakerState {
        BlockingCircuitBreakerState {
            block_on_failure_count,
            retry_period_millis,
            failure_count: Arc::new(AtomicU64::new(0)),
            last_failure: Arc::new(AtomicCell::new(Instant::now())),
            wakers: Arc::new(ArrayQueue::new(2048)),
        }
    }

    fn can_execute(&self) -> bool {
        if self.failure_count.load(Ordering::SeqCst) < self.block_on_failure_count {
            true
        } else {
            let previous = self.last_failure.load();
            let elapsed = previous.elapsed().as_millis();
            if elapsed >= self.retry_period_millis as u128 {
                match self.last_failure.compare_exchange(previous, Instant::now()) {
                    Ok(_) => {
                        log::debug!("Retry!");
                        true
                    }
                    Err(_) => {
                        log::debug!("Time collision prevents retry.");
                        false
                    }
                }
            } else {
                log::debug!("Have not passed elapsed time yet");
                false
            }
        }
    }

    fn register(&self, waker: Waker) {
        self.wakers.push(waker).unwrap();
    }

    fn get_duration_to_next_scheduled_wake_one(&self) -> Duration {
        if self.failure_count.load(Ordering::SeqCst) >= self.block_on_failure_count {
            let time_passed = self.last_failure.load().elapsed().as_millis() as u64;
            let delay = self.retry_period_millis - (time_passed % self.retry_period_millis);
            Duration::from_millis(delay)
        } else {
            Duration::from_millis(self.retry_period_millis)
        }
    }

    fn wake_one(&mut self) {
        if let Some(waker) = self.wakers.pop() {
            waker.wake();
        }
    }

    fn success(&self) {
        self.failure_count.store(0, Ordering::SeqCst);
        while let Some(waker) = self.wakers.pop() {
            waker.wake();
        }
    }

    fn failed(&self) {
        self.failure_count.fetch_add(1, Ordering::SeqCst);
        self.last_failure.store(Instant::now());
    }
}

#[derive(Debug, Clone)]
pub struct BlockingCircuitBreaker {
    config: Arc<BlockingCircuitBreakerState>,
}

impl BlockingCircuitBreaker {
    pub fn new(block_on_failure_count: u64, retry_period_millis: u64) -> BlockingCircuitBreaker {
        let config = BlockingCircuitBreakerState::new(block_on_failure_count, retry_period_millis);

        let mut cfg = config.clone();

        thread::spawn(move || loop {
            let sleep_duration = cfg.get_duration_to_next_scheduled_wake_one();
            log::debug!("waking_thread: sleeping for {:?}!", sleep_duration);
            thread::sleep(sleep_duration);
            log::debug!(
                "waking_thread: done sleeping. There are {} wakers queued. Calling wake_one()",
                cfg.wakers.len()
            );
            cfg.wake_one();
        });

        BlockingCircuitBreaker {
            config: Arc::new(config),
        }
    }

    pub async fn run<C, F, T, E>(&self, closure: C) -> Result<T, Box<E>>
    where
        C: FnOnce() -> F,
        F: std::future::Future<Output = Result<T, Box<E>>>,
        E: Error,
    {
        BlockingCircuitBreakerBefore(self.config.clone()).await;
        let result = (closure)().await;
        match result {
            Ok(ok) => {
                self.config.success();
                Ok(ok)
            }
            Err(err) => {
                self.config.failed();
                Err(err)
            }
        }
    }
}

struct BlockingCircuitBreakerBefore(Arc<BlockingCircuitBreakerState>);

impl Future for BlockingCircuitBreakerBefore {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.0.can_execute() {
            Poll::Ready(())
        } else {
            self.0.register(cx.waker().to_owned());
            log::debug!("before: There are {} wakers in queued", self.0.wakers.len());
            Poll::Pending
        }
    }
}
