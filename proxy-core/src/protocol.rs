use std::error::Error;
use std::fmt;
use std::fmt::Formatter;
use std::time::Duration;

use futures::future::try_select;
use tokio::io;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use tokio::time::timeout;

use crate::protocol::Method::Ping;

/// Describes the type of request the WorkerRouter sends to the WorkerSubscriber
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Method {
    // Any unexpected byte (u8)
    Invalid,
    // Verify worker can be trusted
    // Request: method (u8 1), length (u32), nonce of `length` bytes
    // Response: length (u32), hash of nonce and secret
    Auth,
    // Verify worker is healthy
    // Request: method (u8 2)
    // Response: method (u8 2)
    Ping,
    // Exit protocol mode and start proxying real data
    // Request: method (u8 3)
    StartProxy,
}

impl Method {
    pub fn to_byte(self) -> u8 {
        match self {
            Self::Invalid => 0,
            Self::Auth => 1,
            Self::Ping => 2,
            Self::StartProxy => 3,
        }
    }

    pub fn from_byte(byte: u8) -> Method {
        match byte {
            1 => Self::Auth,
            2 => Self::Ping,
            3 => Self::StartProxy,
            _ => Self::Invalid,
        }
    }
}

impl fmt::Display for Method {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        let message = match self {
            Self::Invalid => "Invalid",
            Self::Auth => "Auth",
            Self::Ping => "Ping",
            Self::StartProxy => "StartProxy",
        };

        fmt::Display::fmt(message, f)
    }
}

#[derive(Debug)]
pub struct ProtocolError {
    message: String,
}

impl Error for ProtocolError {}

impl fmt::Display for ProtocolError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        fmt::Display::fmt(&self.message, f)
    }
}

impl From<String> for ProtocolError {
    fn from(message: String) -> Self {
        ProtocolError { message }
    }
}

impl From<&str> for ProtocolError {
    fn from(message: &str) -> Self {
        ProtocolError {
            message: String::from(message),
        }
    }
}

pub type ProtocolResult<T> = Result<T, Box<dyn Error>>;

#[derive(Debug)]
pub struct RegisteredWorker {
    stream: TcpStream,
    protocol_timeout: Duration,
}

impl RegisteredWorker {
    pub fn new(stream: TcpStream, protocol_timeout: Duration) -> RegisteredWorker {
        RegisteredWorker {
            stream,
            protocol_timeout,
        }
    }

    pub async fn send_ping(&mut self) -> ProtocolResult<()> {
        // Send request
        let ping_byte = Method::Ping.to_byte();
        self.stream.write_u8(ping_byte).await?;

        // Read response
        let pong_byte = timeout(self.protocol_timeout, self.stream.read_u8()).await??;

        // Confirm the returned token matches
        if ping_byte == pong_byte {
            Ok(())
        } else {
            Err(Box::new(ProtocolError::from(
                "Failed to parse response from Ping",
            )))
        }
    }

    pub async fn verify_auth(&mut self, secret: String) -> ProtocolResult<()> {
        // Send request
        let nonce = generate_nonce(64);
        self.stream.write_u8(Method::Auth.to_byte()).await?;
        self.stream.write_u32(nonce.len() as u32).await?;
        self.stream.write_all(nonce.as_slice()).await?;

        // Expected response
        let checksum = one_way_hash(nonce, secret);

        // Read response
        let response_checksum_size =
            timeout(self.protocol_timeout, self.stream.read_u32()).await??;
        let mut response_checksum = vec![0u8; response_checksum_size as usize];
        let response_checksum_future = self.stream.read_exact(response_checksum.as_mut_slice());
        timeout(self.protocol_timeout, response_checksum_future).await??;

        // Verify returned checksum matches expected
        if checksum.eq(&response_checksum) {
            Ok(())
        } else {
            self.stream.write_u8(Method::Invalid.to_byte()).await?;
            self.stream.write_all(b"Failed Auth").await?;
            let _ignored = self.stream.shutdown();
            Err(Box::new(ProtocolError::from(
                "AuthAck provided invalid checksum".to_owned(),
            )))
        }
    }

    pub async fn proxy(&mut self, mut client_stream: TcpStream) -> ProtocolResult<()> {
        self.stream.write_u8(Method::StartProxy.to_byte()).await?;

        // TODO: Figure out how to use the common `transfer`
        let (mut ri, mut wi) = self.stream.split();
        let (mut ro, mut wo) = client_stream.split();

        log::debug!("PROXY: piping connections");
        let client_to_server = Box::pin(io::copy(&mut ri, &mut wo));
        let server_to_client = Box::pin(io::copy(&mut ro, &mut wi));
        let _ignored = try_select(client_to_server, server_to_client).await;

        log::debug!("PROXY: client serve complete");

        Ok(())
    }

    pub async fn shutdown(&mut self) {
        let _ignored = self.stream.shutdown();
    }
}

fn generate_nonce(size: u32) -> Vec<u8> {
    (0..size).map(|_| rand::random::<u8>()).collect()
}

fn one_way_hash(nonce: Vec<u8>, secret: String) -> Vec<u8> {
    use sha2::{Digest, Sha256};
    let mut hasher = Sha256::new();
    hasher.update(nonce);
    hasher.update(secret);
    hasher.finalize().to_vec()
}

pub struct Worker {
    stream: TcpStream,
    to_addr: String,
    secret: String,
    protocol_timeout: Duration,
}

impl Worker {
    pub fn new(
        stream: TcpStream,
        to_addr: String,
        secret: String,
        protocol_timeout: Duration,
    ) -> Worker {
        Worker {
            stream,
            to_addr,
            secret,
            protocol_timeout,
        }
    }

    pub async fn listen_for_work(&mut self) -> ProtocolResult<bool> {
        let method_byte = self.stream.read_u8().await?;
        let method = Method::from_byte(method_byte);
        log::debug!("Recieved method: {}", method);
        match method {
            Method::Ping => self.do_ping().await,
            Method::Auth => self.do_auth().await,
            Method::StartProxy => self.do_proxy().await,
            Method::Invalid => self.do_invalid().await,
        }
    }

    async fn do_ping(&mut self) -> ProtocolResult<bool> {
        self.stream.write_u8(Ping.to_byte()).await?;
        log::debug!("Responded to Ping");
        Ok(true)
    }

    async fn do_auth(&mut self) -> ProtocolResult<bool> {
        let nonce_size = timeout(self.protocol_timeout, self.stream.read_u32()).await??;
        let mut nonce = vec![0u8; nonce_size as usize];
        let read_nonce_future = self.stream.read_exact(nonce.as_mut_slice());
        timeout(self.protocol_timeout, read_nonce_future).await??;

        let hash = one_way_hash(nonce, self.secret.clone());
        self.stream.write_u32(hash.len() as u32).await?;
        self.stream.write_all(&hash).await?;

        log::debug!("Responded to Auth");
        Ok(true)
    }

    async fn do_proxy(&mut self) -> ProtocolResult<bool> {
        log::debug!("Connecting to client {}", &self.to_addr);
        let mut service_stream = TcpStream::connect(&self.to_addr).await?;

        // TODO: Figure out how to use the common `transfer`
        let (mut ri, mut wi) = self.stream.split();
        let (mut ro, mut wo) = service_stream.split();

        log::debug!("piping connections");
        let client_to_server = Box::pin(io::copy(&mut ri, &mut wo));
        let server_to_client = Box::pin(io::copy(&mut ro, &mut wi));
        let _ignored = try_select(client_to_server, server_to_client).await;

        log::debug!("Done proxying. Worker will terminate");
        Ok(false)
    }

    async fn do_invalid(&mut self) -> ProtocolResult<bool> {
        let mut message = String::new();
        self.stream.read_to_string(&mut message).await?;
        panic!("Invalid protocol error: {}", message)
    }
}

#[cfg(test)]
mod tests {
    use crate::protocol::Method;

    #[test]
    fn test_method_byte_encoding() {
        assert_eq!(Method::Auth, Method::from_byte(Method::Auth.to_byte()));
        assert_eq!(Method::Ping, Method::from_byte(Method::Ping.to_byte()));
        assert_eq!(
            Method::StartProxy,
            Method::from_byte(Method::StartProxy.to_byte())
        );
        assert_eq!(Method::Invalid, Method::from_byte(0));
        assert_eq!(Method::Invalid, Method::from_byte(4));
    }
}
